from django.test import TestCase, Client
from django.urls import resolve
from .views import landing_page
from .models import Status
import datetime
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import unittest
import time
from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_use_functione(self):
        response = resolve('/')
        self.assertEqual(response.func, landing_page)
    
    def test_use_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_model_can_create_status(self):
        new_status = Status.objects.create(status='mood')
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

    def test_model_can_create_status_with_status_as_title(self):
        new_status = Status.objects.create(status='mood')
        self.assertEqual(str(new_status), new_status.status)

    def test_can_save_POST(self):
        response = self.client.post('/add_status/', data={
            'status' : 'mood'
        })
        status_count = Status.objects.all().count()
        self.assertEqual(status_count, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_status_is_shown(self):
        response = self.client.post('/add_status/', data={
            'status' : 'mood'
        })
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('mood', html_response)


class NewVisitorTest(StaticLiveServerTestCase):
    def setUp(self):
        op = Options()
        op.add_argument('--dns-prefetch-disable')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=op)
        return super().setUp()

    def tearDown(self):
        self.browser.quit()
        return super().tearDown()
        

    def test_input_a_status_and_make_it_shown(self):
        self.browser.get('%s%s' % (self.live_server_url, '/'))
        time.sleep(2)
        input_box = self.browser.find_element_by_id('id_status')
        input_box.send_keys('mood')
        input_box.submit()
        time.sleep(2)
        self.assertIn('mood', self.browser.page_source)
