from django.urls import path
from . import views

app_name = 'LandingPage'

urlpatterns = [
    path('', views.landing_page, name='landing_page'),
    path('add_status/', views.add_status, name='add_status')
]