from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def landing_page(request):
    status = Status.objects.all().values()
    context = {
        'form' : StatusForm,
        'status' : status
    }
    return render(request, 'landing.html', context)

def add_status(request):
    if StatusForm(request.POST).is_valid():
        new_status = Status.objects.create(status=request.POST['status'])
    return redirect('/')
