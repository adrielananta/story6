from django import forms

class SearchBar (forms.Form):
    query = forms.CharField(min_length=1)