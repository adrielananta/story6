document.getElementById("search-bar").addEventListener("submit", function (event) {
    event.preventDefault();
    document.querySelector(".body").innerHTML = '<div class="loader"></div>'
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (JSON.parse(this.response).totalItems == 0) {
                document.querySelector(".body").innerHTML = "None";
            } else {
                document.querySelector(".body").innerHTML = JSON.parse(this.response).items.map((item, index, itemsArray) => {
                    return `<tr> 
                            ${item.volumeInfo.imageLinks ? `<td><img src="${item.volumeInfo.imageLinks.thumbnail}"style="max-width: 18rem" alt="..."></td>` : `<td><img src="https://upload.wikimedia.org/wikipedia/commons/b/b9/No_Cover.jpg" style="max-width: 18rem" alt="..."></td>`}
                            <td><h5>${item.volumeInfo.title}</h5></td>
                            <td><p><small class="text-muted">${item.volumeInfo.authors ? item.volumeInfo.authors.join(",") : "No author(s) found"}</small></p></td>
                            </tr>`;
                }).join(" ");
            }
        } else if (this.readyState == 4 && (this.status == 400 || this.status == 404)) {
            document.querySelector(".body").innerHTML = "Tidak dapat ditemukan"
        }
    };
    param = document.getElementById("id_query").value.trim().split(" ").join("_");
    xhttp.open("GET", `cari/${param}/`, true);
    xhttp.setRequestHeader("X-CSRFToken", document.getElementsByName('csrfmiddlewaretoken')[0].value);
    // console.log(document.getElementsByName('csrfmiddlewaretoken')[0].value)
    xhttp.send();})
