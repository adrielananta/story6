from django.test import TestCase
from django.urls import resolve
from django.http import JsonResponse
from .views import *
import json
import requests

class UnitTest(TestCase):
    def test_url_daftar_exist(self):
        response = self.client.get('/storyDelapan/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = self.client.get('/storyDelapan/')
        self.assertTemplateUsed(response, 'StoryDelapan.html')

    def test_view_function_used(self):
        response = resolve('/storyDelapan/')
        self.assertEqual(response.func, index)

    
