from django.urls import path
from .views import *

app_name = 'StoryDelapan'

urlpatterns = [
    path('', index, name='index'),
    path('cari/<str:param>/', cari, name='cari'),
]