from django.shortcuts import render
from django.http import JsonResponse
import requests
import json
from .forms import SearchBar


# Create your views here.

def index(request):
    context = {
        'form' : SearchBar()
    }
    return render(request, 'StoryDelapan.html', context)

def cari(request, param):
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + param).json()
    return JsonResponse(json)