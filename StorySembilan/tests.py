from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import story_sembilan_page, funct_login, funct_logout
from django.contrib.auth.models import User


# Create your tests here.

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/storySembilan/')
        self.assertEqual(response.status_code, 200)

    def test_use_template(self):
        response = self.client.get('/storySembilan/')
        self.assertTemplateUsed(response, 'storysembilan.html')

    def test_use_function(self):
        response = resolve('/storySembilan/')
        self.assertEqual(response.func, story_sembilan_page)

    def test_use_login_funct(self):
        response = resolve('/storySembilan/login/')
        self.assertEqual(response.func, funct_login)

    def test_use_logout_funct(self):
        response = resolve('/storySembilan/logout/')
        self.assertEqual(response.func, funct_logout)

    def test_login(self):
        user = User.objects.create_user("Adriel", "adriel.gian@ui.ac.id", "nonutnovember")
        response = self.client.post('/storySembilan/login/', data={
            'username' : 'Adriel',
            'password' : 'nonutnovember'
        })
        self.assertEqual(response.status_code, 302)