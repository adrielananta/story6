from django.urls import path, include
from .views import story_sembilan_page, funct_login, funct_logout

app_name="Story9"
urlpatterns = [
    path('', story_sembilan_page, name="story9"),
    path('login/', funct_login, name="login"),
    path('logout/', funct_logout, name="logout")
]
