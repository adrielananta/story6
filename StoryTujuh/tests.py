from django.test import TestCase, Client
from django.urls import resolve
from .views import story_tujuh_page
#from .models import Status
#import datetime
#from selenium import webdriver
#from django.contrib.staticfiles.testing import StaticLiveServerTestCase
#import unittest
#import time
#from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/storyTujuh/')
        self.assertEqual(response.status_code, 200)

    def test_use_function(self):
        response = resolve('/storyTujuh/')
        self.assertEqual(response.func, story_tujuh_page)
    
    def test_use_template(self):
        response = self.client.get('/storyTujuh/')
        self.assertTemplateUsed(response, 'storytujuh.html')

    def test_use_css(self):
        self.assertContains(Client().get('/storyTujuh/'), 'storyTujuh.css')