from django.urls import path
from .views import story_tujuh_page

app_name = 'StoryTujuh'

urlpatterns = [
    path('', story_tujuh_page, name="story_tujuh_page")
]